<?php
declare(strict_types=1);

use Phalcon\Di\FactoryDefault;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    $composer_path='../vendor/autoload.php';
    if(file_exists($composer_path)) {
        require '../vendor/autoload.php';
    } else {
        $throwables = new \Exception("composer package went to the rainbow-bridge");

        throw $throwables;
    }
} catch(\Exception $e) {
    echo "<pre>";
    echo $e->getMessage();
    echo "</pre>";
    exit(0);
}

try {
    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();

    /**
     * Environment variables
     */
    $dotenv = Dotenv\Dotenv::createImmutable( __DIR__ . '/../');
    $dotenv->load();

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Handle routes
     */
    include APP_PATH . '/config/router.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle($_SERVER['REQUEST_URI'])->getContent();
} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
