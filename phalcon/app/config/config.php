<?php

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new \Phalcon\Config([
    'database' => [
        'adapter'  => (!empty($_ENV['DB_ADAPTER']))?$_ENV['DB_ADAPTER']:'Mysql',
        'host'     => (!empty($_ENV['DB_HOST']))?$_ENV['DB_HOST']:'localhost',
        'username' => (!empty($_ENV['DB_USERNAME']))?$_ENV['DB_USERNAME']:'root',
        'password' => (!empty($_ENV['DB_PASSWORD']))?$_ENV['DB_PASSWORD']:'',
        'dbname'   => (!empty($_ENV['DB_NAME']))?$_ENV['DB_NAME']:'test',
        'charset'  => 'utf8',
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'baseUri'        => '/',
    ]
]);
